@extends('layout')
@section('content')
<div class="container">
  <div class="row text-center">

    <div class="col-md-4 col-md-offset-4">
      <div class="panel panel-success">
        <div class="panel-heading">
          <h4>
            Total Requests:
            {{count($reg_details)}}
          </h4>
        </div>
      </div>
    </div>

  </div>
  <div class="row">
    <table id="reg_requests" class="table table-striped table-reponsive table-bordered" width="100%" cellspacing="0">
      <thead>
        <tr>
          <td>Name</td>
          <td>Category</td>
          <td>Email</td>
          <td>Mobile</td>
          <td>Comments</td>
        </tr>
      </thead>
      <tbody>
        @foreach($reg_details as $index=>$each)
          <tr>
            <td>
              {{$each->user_name}}
            </td>
            <td>
              {{$each->user_type_name}}
            </td>
            <td>
              {{$each->email}}
            </td>
            <td>
              {{$each->mobile_number}}
            </td>
            <td>
              {{$each->comments}}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@push('page_scripts')
<script src="{{asset('dist/datatables/datatables.min.js')}}"></script>
<script>
$(document).ready(function() {
    $('#reg_requests').DataTable({
      dom:  "<'row'<'col-sm-3'B><'col-sm-3'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      buttons: ['copy', 'excel'],
      "order": []
    });

});
</script>
@endpush
