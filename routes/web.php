<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::resource('/payment_on_request', 'RegistrationController');
Route::resource('/', 'RegistrationController');

Route::get('/payment_status', function () {
    return redirect('/');
});

Route::match(['get', 'post'],'/payment_status', 'RegistrationController@payment_status');

Route::match(['get', 'post'],'/registrations', 'RegistrationController@view_registrations');

Route::match(['get', 'post'],'/update_tickets','RegistrationController@update_tickets');

Route::put('/update_tickets','RegistrationController@update_category');

Route::match(['get', 'post'],'/','RegistrationController@students_for_later');

Route::get('/pay_money/{verification_code}','RegistrationController@pay_money');

Route::match(['get', 'post'],'/reg_requests', 'RegistrationController@view_reg_requests');
